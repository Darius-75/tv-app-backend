import { Injectable } from '@nestjs/common';
import axios from 'axios';
// import { Repository } from 'typeorm';

@Injectable()
export class MovieService {
  private apiKey = 'bbf24e8e0f0e320e4b9b2f3df6f19031';
  // private movieRepository: Repository<any>;

  async getMoviesPopular() {
    const url = `https://api.themoviedb.org/3/movie/popular?api_key=${this.apiKey}`;
    const response = await axios.get(url);
    // this.movieRepository.save(response.data.results)
    return response.data.results;
  }
  

  async getMoviesUpcoming() {
    const url = `https://api.themoviedb.org/3/movie/upcoming?api_key=${this.apiKey}`;
    const response = await axios.get(url);
    return response.data.results;
  }

  async getMovie(id: number) {
    const url = `https://api.themoviedb.org/3/movie/${id}?api_key=${this.apiKey}`;
    const response = await axios.get(url);
    return [response.data]; // Mettez le film dans un tableau
  }
  
}
