import { Controller, Get, Param } from '@nestjs/common';
import { MovieService } from './movie.service';

@Controller('/movies')
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @Get("/popular")
async getMoviesPopular() {
  return this.movieService.getMoviesPopular();
}


@Get("/upcoming")
async getMoviesUncoming() {
  return this.movieService.getMoviesUpcoming();
}
  

  @Get(':id')
  async getMovie(@Param('id') id: number) {
    return this.movieService.getMovie(id);
  }
}
