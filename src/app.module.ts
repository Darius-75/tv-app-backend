import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import  { TypeOrmModule } from '@nestjs/typeorm';
import { MovieController } from './movie/movie.controller';
import { MovieService } from './movie/movie.service';

//connection a la database postgres

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',  
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '123',
      database: 'my-app',
      autoLoadEntities: true,
      synchronize: true, 
})],
controllers: [AppController, MovieController],
providers: [AppService, MovieService],

  
})
export class AppModule {}


